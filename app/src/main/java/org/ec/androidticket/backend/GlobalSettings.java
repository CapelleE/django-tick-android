package org.ec.androidticket.backend;

public class GlobalSettings
{
    public static final String DEVELOPMENT_WEBSERVICE_ADDRESS       = "http://46.101.155.148:80/";
    public static final String PRODUCTION_WEBSERVICE_ADDRESS       = "http://ticketingplatform.no-ip.org:80/";
    public static final String CREDENTIAL_COOKIE_FILENAME   = "cookie";

    public static String getProductionWebserviceAddress()
    {
        return PRODUCTION_WEBSERVICE_ADDRESS;
    }

    public static String getDevelopmentWebserviceAddress()
    {
        return DEVELOPMENT_WEBSERVICE_ADDRESS;
    }

    public static String getCredentialCookieFilename()
    {
        return CREDENTIAL_COOKIE_FILENAME;
    }
}
